/**
 * 
 */
package com.radicalTech.Assign11_12;
import java.util.*;

/**
 * @author Manoj Kumar
 *
 */
public class SystemProp {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Properties p= new Properties(System.getProperties());			
		System.out.println("Java version: "+p.getProperty("java.version"));
		System.out.println("Java Runtime version: "+p.getProperty("java.runtime.version"));
		System.out.println("Java Home: "+p.getProperty("java.home"));
		System.out.println("Java vender: "+p.getProperty("java.vendor"));
		System.out.println("Java vender url : "+p.getProperty("java.vendor.url"));
		System.out.println("Java class path : "+p.getProperty("java.class.path"));
		System.out.println("***For swap see example 10 : ");

	}

}
