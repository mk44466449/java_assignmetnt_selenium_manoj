/**
 * 
 */
package com.radicalTech.Assign5_6;

import java.util.Scanner;

/**
 * @author Manoj Kumar
 * example for variable and operator combine
 *
 */

class Student {
	public String name;//member variable
	public int id; //instance variable	
	public static int barcode;//class variable
	Student s1;//user define var
	
}
public class VariableOperator {
	
	static int add(int a,int b){
		int result=a+b;	//local variable
		System.out.println("addition result is :: "+result);
		return result;
	}
	static int sub(int a,int b){
		int result=a-b;	
		System.out.println("subtract result is :: "+result);	
		return result;
	}
	static int mul(int a,int b){
		int result=a*b;	
		System.out.println("multiplication result is :: "+result);	
		return result;
	}
	static int div(int a,int b){
		int result=a/b;
		System.out.println("divide result is :: "+result);	
		return result;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new VariableOperator().add(45,34); //parameter send by value with anonymous obj
		new VariableOperator().sub(45,34);
		new VariableOperator().mul(45,34);
		new VariableOperator().div(45,34);		
		
		//create object for student class
		Student studentObj=new Student();
		studentObj.name="Manoj Kumar";
		System.out.println("your name is ::"+studentObj.name);
		studentObj.id=1899;
		System.out.println("your id is::"+studentObj.id);
		studentObj.barcode=234567;
		System.out.println("your barcode is::"+studentObj.barcode);
		System.out.println("******uses of assignment operator******");
		
		System.out.println("Enter your Age ::(19 is too low,20-40yrs)");
		Scanner sc=new Scanner(System.in);		
		int age=sc.nextInt();
		 if(age==19){
				System.out.println("too low age");
		 }
		 if(age<19){
				 System.out.println("not eligible");
				 System.exit(-1);
		 }		
		 if((age>=20) && (age<=40)){
			System.out.println("Your are eligible for JAVA Test!!!");			
		 } 
		
		 else{
			System.out.println("your age is out of rage,sorry you can't take java test");
			System.exit(-1);
		 }
		 
		 
		 System.out.println("Enter roll no.(1-98, if you put 99 try it");
		 Scanner sc2=new Scanner(System.in);		
		 int roll = sc2.nextInt();
		 if(roll !=99){
			 int roll1=roll;
			 System.out.println("roll is not equal 99 ");
		 }
		 
		 System.out.println(" Now Enter your grade in Java ::(AA,BB,CC)");
		 Scanner sc1=new Scanner(System.in);		
		 String grade = sc1.nextLine();
		
		 
		 if(grade.equals("AA") || grade.equals("aa")){
			System.out.println("you are in topper Group");
			}
		 
		   else if(grade.equals("BB")  || grade.equals("bb") || grade.equals("CC") || grade.equals("cc")){
				 System.out.println("you are Pass in Examination");
			 }
				
			 else{
				 System.out.println("Fail !! better luck for next time ");
			 }

	}

}
