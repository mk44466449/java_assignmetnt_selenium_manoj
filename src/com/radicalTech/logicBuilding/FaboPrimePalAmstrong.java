/**
 * 
 */
package com.radicalTech.logicBuilding;

/**
 * @author Manoj Kumar
 *
 */
public class FaboPrimePalAmstrong {
	
	public static void fibonacci(int n){
		int a=0,b=1,c,i=1;
		System.out.print("\t"+a);
		System.out.print("\t"+b);
		while(i<=n-2){
			c=a+b;
			System.out.print("\t"+c);
			a=b;
			b=c;
			i++;
			
		}
		
	}
	static void primeno(int n){		
		boolean b1=true;
		System.out.println("\n");
		
		for(int i=2;i<=n/2;i++){			
			if(n%i == 0){
				b1=false;
				break;				
			}
		}
		if(b1)
			System.out.println("No :"+n+" is a prime no.");
		else
			System.out.println("No :"+n+" is not a prime no.");
		
		
	}
	static void palindrom(int n){
		int rev = 0;
		int num=n;
		while(n!=0){
			int digit = n%10;
			rev = rev*10+digit;
			n = n/10;			
		}
		if(num == rev)
			System.out.println("Palindrom");
		else
			System.out.println("Not Palindrom");
		
	}
	static void amstrong(int n){
		int rem,arm=0;	
		int num1=n;
		while(n!=0){
			 rem = n%10;
			 arm = arm+(rem*rem*rem);
			 n=n/10;
			
		}if( num1==arm)
			System.out.println("Amstrong");
		else
			System.out.println("Given no. is not amstrong no");
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		fibonacci(10);
		primeno(11);
		palindrom(12221);
		amstrong(151);

	}

}
