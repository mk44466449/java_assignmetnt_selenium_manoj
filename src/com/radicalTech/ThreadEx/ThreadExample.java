/**
 * 
 */
package com.radicalTech.ThreadEx;

/**
 * @author Manoj Kumar
 *
 */
class RunnableThreadEx implements Runnable{
	public void run(){
		System.out.println("Current Runnable thread name is :: "+Thread.currentThread().getName());
	}
	
}
public class ThreadExample extends Thread{

	/**
	 * @param args
	 */
	public void run(){
		System.out.println(Thread.currentThread().getName());
	}
	public void manoj(){
		System.out.println(Thread.currentThread().getName());
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ThreadExample te=new ThreadExample();
		String name = te.currentThread().getName();
		System.out.println("Get Priority :: "+te.getPriority());		
		System.out.println("Thread name is :: "+name);
		te.start();
		te.run();
		te.manoj();
		RunnableThreadEx rte=new RunnableThreadEx();
		rte.run();
		

	}

}
