/**
 * 
 */
package com.radicalTech.AccessModifier;

import java.util.Scanner;

import javax.management.RuntimeErrorException;

/**
 * @author Manoj Kumar
 *
 */
public class TestPublic {
	
	public void publicOutside(){
		System.out.println("This method is public and access everywhere");
		//privateInside();
	}
	private void privateInside(){
		System.out.println("This method is private and access Within the class only");
	}
	protected void protactedMethod1(){
		System.out.println("This is protacted method");
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TestPublic tp=new TestPublic();
		tp.privateInside();
		int a=100;
		int b=0;
		int c=0;
		int arr[]={10,20,30,40};
		
		System.out.println("befor result "+c);
		try{
			 c=a/b;
			 
		}
		catch(ArithmeticException ae){
			System.out.println("Arthmatic exception Found :: "+ae);
		}

		try{
			System.out.println("Array value of a[0]"+arr[-1]);
			 System.out.println("Value of arr[6] is :: "+arr[6]);
		}
		
		catch(ArrayIndexOutOfBoundsException aiobe){
			System.out.println("Array out of bond prob...."+aiobe);
		}
			catch(Exception e){
			System.out.println("other kind of exception"+e);
			
		}
		finally
		{
			System.out.println("Welcome,have some exception but fine");
		}
		Scanner sc =new Scanner(System.in);
		System.out.println("Enter age ::");
		int age = sc.nextInt();
		if(age>=18){
			System.out.println("Eligible");
			
		}else{
			throw new RuntimeException( "Age less then 16");
		}

	}

}
