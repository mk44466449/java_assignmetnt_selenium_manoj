/**
 * 
 */
package com.radicalTech.Collections;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;

/**
 * @author Manoj Kumar
 *
 */
public class CollectionExample {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// to store any kind of data 
		ArrayList al=new ArrayList();
		al.add("manoj");
		al.add("Raushan");
		al.add(564);
		al.add(3.5);
		al.add(63l);		
		System.out.println("data store are :: "+al);
		System.out.println("index of :: "+al.indexOf("manoj"));
		System.out.println("Get Data :: "+al.get(1));
		System.out.println("Size of array List is ::"+al.size());
		for(int i = 0;i<al.size();i++){
			System.out.println("Elements are ::"+al.get(i));
		}
//		Iterator it=al.listIterator();
//		while(it.hasNext()){
//			//String str=(String) it.next();//due to mixup data
//			//System.out.println("Data are ::"+str);
//		}
		List<String> al2=new ArrayList<String>();
		al2.add("manoj");
		al2.add("Raushan");
		al2.add("Bhushan");
		al2.add("Ram");
		ListIterator<String> itL = al2.listIterator();
		//Iterator itL=al2.listIterator();
		while(itL.hasPrevious()){
			//String itLstr= itL.hasPrevious();
			System.out.println("Result :: "+itL.previous());
		}
		
		HashMap<String,String> hm=new HashMap<String,String>();
		hm.put("key1", "manoj");
		hm.put("key2", "raushan");
		hm.put("key3", "bhushan");
		System.out.println(hm.get("key1"));
		System.out.println(hm);
		Set <String> keys=new HashSet<String>(); 
		Iterator<String> itr=keys.iterator();
		while(itr.hasNext()){
			String str=itr.next();
			System.out.println("Result :: "+str);
			
		}
		
	}

}
