/**
 * 
 */
package com.radicalTech.ExceptionThThrows;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Scanner;

/**
 * @author Manoj Kumar
 *
 */

//User define Exception steps
/*
 *1.) write  your own exception  JAVA class by extending java.lang.Exception or java.lang.RuntimeException
 *2.) Declare instance variable inside Exception class(if required)
 *3.) write default constructor
 *4.) write argument constructor (if required)
 *5.) override getMessage() method
 *6.) override toString() method
 *7.) override equals() method
 * 
 * */
class AccountNotFoundException extends Exception{
	int accNo;
	
	AccountNotFoundException(){
		
	}
	AccountNotFoundException(int accNo){
		this.accNo=accNo;
		
	}
	
	public String getMessage(int accNo){
		this.accNo=accNo;
		String msg = "Account No: "+accNo+"Not Available.";
		return msg;
		
	}
	public String toString(){
		String str="AccountNotFoundException:"+accNo+"Not Available";
		return str;
	}
	
}
class HelloThrows{
	void m1() throws IOException{
		System.out.println("This is m1() to check throws");
		m2();
	}
	void m2()throws IOException,ArithmeticException{
		{
			int c = 10/0;
			System.out.println("This is m2()"+c);
			m3();
		}
		
	}
	void m3() throws IOException{
		System.out.println("This is m3()");
		try{
			m4();
		}catch(SQLException sqe){
			System.out.println(sqe);
		}
	}
	void m4() throws IOException,SQLException
	{
		System.out.println("This is m4()");
		m5();
	}
	void m5() throws IOException{
		System.out.println("This is m5()");
		throw  new IOException();
	}
}
public class ExceptionAllExmpl {

	/**
	 * @param args
	 */
	public static void main(String[] as) {
		
		HelloThrows ht=new HelloThrows();
		try{
			ht.m1();
			
		}catch(Exception e){
			e.printStackTrace();
		}
		try{
			ht.m5();
		}catch(Exception e1){
			e1.printStackTrace();
			
		}	
		
		
		System.out.println("Enter Account No.");
		Scanner sc=new Scanner(System.in);
		
		try{
			
			int accNo = sc.nextInt();
			
			if(accNo>100){
				AccountNotFoundException anfe = new AccountNotFoundException();
				throw anfe;
				
			}else{
				System.out.println("OK, Looks Good");
			}
		}catch(Exception e){
			System.out.println(e.getMessage());
			System.out.println(e);
		}
		// TODO Auto-generated method stub

	}

}
