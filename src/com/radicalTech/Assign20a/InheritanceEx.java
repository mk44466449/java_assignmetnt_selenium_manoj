/**
 * 
 */
package com.radicalTech.Assign20a;

/**
 * @author Manoj Kumar
 *
 */

class A{
	A(){
		System.out.println("Constructor A....");
		
	}
	
}
class B{
	B(){
		System.out.println("Constructor A....");
		
	}
	
}
class C extends A{
	B b=new B();
}
public class InheritanceEx {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		C c=new C();

	}

}
