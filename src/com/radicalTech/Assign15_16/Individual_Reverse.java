/**
 * 
 */
package com.radicalTech.Assign15_16;

import java.util.Scanner;

/**
 * @author Manoj Kumar
 *
 */
public class Individual_Reverse {

	/**
	 * @param args
	 */
	static void individualSum(){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter only two no. for sum of individual :: ");
		int digit = sc.nextInt();
		int div = 0;
		while(digit!=0){
			int rem = digit % 10;
			div = digit / 10;
			int sum = rem + div;	
			System.out.println("sum is : "+ sum);	
			break;
		}
		
		
	}
	static void reverseString(){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter String :: ");
		String str = sc.nextLine();
		int length = str.length();
		for(int i=0;i<str.length();i++){
			System.out.print(str.charAt(i));
			
		}
		System.out.println("\nReverse");
		for(int i = length-1;i>=0;i--){
			
			System.out.print(str.charAt(i));
		}
		
	}
	public static void main(String[] args) {
		
		individualSum();
		reverseString();

	}

}
