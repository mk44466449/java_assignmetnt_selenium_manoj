/**
 * 
 */
package com.radicalTech.InterfaceEx;

import com.radicalTech.logicBuilding.FaboPrimePalAmstrong;




/**
 * @author Manoj Kumar
 *
 */
 interface Bank {
	void pay();
	void receive();
	

}

public class BankClass implements Bank {

	/**
	 * @param args
	 */
	
@Override
	  public void pay() {
		// TODO Auto-generated method stub
		System.out.println("Payment successfully");
		
	}
@Override
	 public void receive(){
		// TODO Auto-generated method stub
		System.out.println("Got payments");
		
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		BankClass b = new BankClass();
		b.pay();
		b.receive();
		//out side of package is public
		FaboPrimePalAmstrong fpo = new FaboPrimePalAmstrong();
		fpo.fibonacci(10);
	
		
		
	}

}
