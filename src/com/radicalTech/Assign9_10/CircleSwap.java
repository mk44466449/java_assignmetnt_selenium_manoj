/**
 * 
 */
package com.radicalTech.Assign9_10;

/**
 * @author Manoj Kumar
 *
 */
public class CircleSwap {
	static void swap(int a,int b){
		System.out.println("before swap ::A= "+a);
		System.out.println("before swap ::B= "+b);
		a=a+b;
		b=a-b;
		a=a-b;
		System.out.println("After swap ::A= "+a);
		System.out.println("After swap ::B= "+b);
		
	}
	static void circleAreaPeri(float r){
		float area =22/7*r*r;
		float perimeter= 2*22/7*r;
		System.out.println("Area of circle : "+area);
		System.out.println("perimeter of circle : "+perimeter);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		swap(50,100);
		circleAreaPeri(23.0f);

	}

}
