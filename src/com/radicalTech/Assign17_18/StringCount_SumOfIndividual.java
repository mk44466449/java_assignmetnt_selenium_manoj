/**
 * 
 */
package com.radicalTech.Assign17_18;

import java.util.Scanner;

/**
 * @author Manoj Kumar
 *
 */
public class StringCount_SumOfIndividual {
	void sumOfIndividual(){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter No to sum:");
		int sumDigit = sc.nextInt();
		int sum = 0;
		for(int i=0;i<=sumDigit;i++){
			sum = sum+i;		
			
		}
		System.out.println("Entered digit is: "+sumDigit+" And sum of its : "+sum);
		System.out.println("******** Individual sum ********");
		int finalSum = 0;
		int digit;
		while(sumDigit!=0){
			digit = sumDigit % 10;
			sumDigit = sumDigit / 10;
			finalSum = finalSum + digit;
			
		}
		System.out.println("Sum of digits : "+finalSum);
		System.out.println("******** End sum ********");
		
		
	}
	void stringCountOperation(){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter String:");
		String inputString=sc.nextLine();
		//convert into charArray
		char convertCharArr[]=inputString.toCharArray();
		//initlize variable to count
		int letter = 0;
		int space = 0;
		int number = 0;
		int other = 0;
		for(int i = 0;i<convertCharArr.length;i++){
			if(Character.isLetter(convertCharArr[i]))
				letter++;
			if(Character.isSpaceChar(convertCharArr[i]))
				space++;
			if(Character.isDigit(convertCharArr[i]))
				number++;
			else
				other++;
			
		}
		//Aa Kiu,I swd sjuei 236587. GH kiu: sieo?? 25.33
		System.out.println("Total letter are : "+letter);
		System.out.println("Total Spaces are : "+space);
		System.out.println("Total digits are : "+number);
		System.out.println("Total others are : "+other);

	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		StringCount_SumOfIndividual stringCountSumofIndividual = new StringCount_SumOfIndividual();
		stringCountSumofIndividual.sumOfIndividual();
		stringCountSumofIndividual.stringCountOperation();

	}

}
