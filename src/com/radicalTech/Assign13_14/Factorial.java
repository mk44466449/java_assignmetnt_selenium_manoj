/**
 * 
 */
package com.radicalTech.Assign13_14;

import java.util.Scanner;

/**
 * @author Manoj Kumar
 *
 */
public class Factorial {

	/**
	 * @param args don't know about recursion
	 */
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter no. ::");
		int n = sc.nextInt();
		int j=1;
		for(int i=n;i>1;i--){
			j=j*i;
			System.out.print("\t"+j);
		}
		
	}

}
