/**
 * 
 */
package com.radicalTech.Assign1_2;

import java.util.Scanner;

/**
 * @author Manoj Kumar
 *
 */
public class OddEvenArrayItem {

	/**
	 * @param args
	 * Traverse an integer array to find out if the no.are even or odd display 	 
	 *appropriate message
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Enter the Size of Array :: ");
		Scanner sc=new Scanner(System.in);
		int size = sc.nextInt();
		int storeArrayElement[]=new int[size];
		int max=storeArrayElement[0],index=0;
		
		int i=0,j=0,k=0;
	
		System.out.println("Now Entered Elements :::");
		for( i=0;i<size;i++){
			 storeArrayElement[i]=sc.nextInt();
		}
		System.out.println("Your Elements are ::");
		for( int Output:storeArrayElement){
			System.out.print("\t"+Output);
			
		}
		for( j=0;j<storeArrayElement.length;j++){			
			if(storeArrayElement[j]>max){
				max=storeArrayElement[j];
				index=j;				
			}
			
		}
		
		int min=storeArrayElement[0];
		for( k=0;k<storeArrayElement.length;k++){			
			if(storeArrayElement[k]<min){
				min=storeArrayElement[k];
				index=k;				
			}
			
		}
		System.out.println();
		System.out.println("maximum is\n :: "+max+"at index ::"+index);		
		System.out.println("minimum is\n :: "+min+"at index ::"+index);
		
		System.out.println("Now ODD and Even Found");
		for(int l=0;l<storeArrayElement.length;l++){
			if(storeArrayElement[l]%2==0){
				int odd = storeArrayElement[l];
				System.out.println("Even numbers are ::"+odd );
				
			}else if(storeArrayElement[l]%2!=0){
				int even = storeArrayElement[l];
				System.out.println("Odd numbers are ::"+even);
			}
			
		}

	}

}
